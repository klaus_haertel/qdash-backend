<?php

abstract class Utilities {
	public static function makeTime($year_mod = 0,$month_mod = 0,$day_mod = 0,$hr_mod = 0,$min_mod = 0,$sec_mod = 0) {
		$thisyear = date("Y");
		$thismonth = date("m");
		$thisday = date("d");
		
		$thishour = date("H");
		$thismin = date("i");
		$thissec = date("s");
	
		return date("Y-m-d H:i:s", mktime($thishour + $hr_mod, $thismin + $min_mod, $thissec + $sec_mod, 
									$thismonth + $month_mod, $thisday + $day_mod, $thisyear + $year_mod));
	}
	
    function array_column($input, $column_key, $index_key = null)
    {
        if ($index_key !== null) {
            // Collect the keys
            $keys = array();
            $i = 0; // Counter for numerical keys when key does not exist
            
            foreach ($input as $row) {
                if (array_key_exists($index_key, $row)) {
                    // Update counter for numerical keys
                    if (is_numeric($row[$index_key]) || is_bool($row[$index_key])) {
                        $i = max($i, (int) $row[$index_key] + 1);
                    }
                    
                    // Get the key from a single column of the array
                    $keys[] = $row[$index_key];
                } else {
                    // The key does not exist, use numerical indexing
                    $keys[] = $i++;
                }
            }
        }
        
        if ($column_key !== null) {
            // Collect the values
            $values = array();
            $i = 0; // Counter for removing keys
            
            foreach ($input as $row) {
                if (array_key_exists($column_key, $row)) {
                    // Get the values from a single column of the input array
                    $values[] = $row[$column_key];
                    $i++;
                } elseif (isset($keys)) {
                    // Values does not exist, also drop the key for it
                    array_splice($keys, $i, 1);
                }
            }
        } else {
            // Get the full arrays
            $values = array_values($input);
        }
        
        if ($index_key !== null) {
            return array_combine($keys, $values);
        }
        
        return $values;
    }
    
	function curl_get($url, array $get = NULL, array $options = array()) 
	{    
		if(!is_null($get)) {
		    $opt_url = $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get);		
		}
		else {
			$opt_url = $url. (strpos($url, '?') === FALSE ? '?' : '');
		}
	    
	    $defaults = array( 
	        CURLOPT_URL => $opt_url, 
	        CURLOPT_HEADER => 0, 
	        CURLOPT_RETURNTRANSFER => TRUE, 
	        CURLOPT_TIMEOUT => 4 
	    ); 
	    
	    $ch = curl_init(); 
	    curl_setopt_array($ch, ($options + $defaults)); 
	    if( ! $result = curl_exec($ch)) 
	    { 
	        trigger_error(curl_error($ch)); 
	    } 
	    curl_close($ch); 
	    return $result; 
	} 
	
	function getConfig() {
		return (array)json_decode(Utilities::curl_get('http://api.signalq.ca/Config'));
	}
	
	function getSlots($update, $i_customer) {
		//construct URL and GET parameters
		$url = 'http://api.signalq.ca/Slots/'.$i_customer.'/';
		$gets = array('updates' => $update);
		//perform API calls
		return (array)json_decode(Utilities::curl_get($url, $gets));
	}
	
}

?>