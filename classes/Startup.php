<?php

include_once(__DIR__ .'/../config/widgets.php');
include_once(__DIR__ .'/Utilities.php');
include_once(__DIR__ .'/Widget.php');

foreach (glob(__DIR__ ."/../widgets/*.php") as $filename)
{
    include $filename;
}

//include custom widgets
foreach (glob(__DIR__ ."/../widgets/Custom/*/*.php") as $filename)
{
    include $filename;
}


$config = array_merge(Utilities::getConfig(), $config);
date_default_timezone_set($config['system_timezone']);

?>