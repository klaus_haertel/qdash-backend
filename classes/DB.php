<?php

class DB 
{
	private static $_conn = null; 
	
	public function __construct($db_type) {
		
		global $config;
		
		//$ini_array = parse_ini_file(__DIR__ .'/../config/db.ini', true);
		
		ini_set('pdo_mysql.default_socket', '/tmp/mysql.sock');
		
		/**
		$pdo_connect = "mysql:host=".$ini_array[$db_type]['db_host'].
						";port=".$ini_array[$db_type]['db_port'].
						";dbname=".$ini_array[$db_type]['db_name'];
		**/
		
		$new_pdo = "mysql:unix_socket=/tmp/mysql.sock;dbname=porta-billing";
		
		try {
		    self::$_conn = new PDO($new_pdo, $config['ps_db_user'], $config['ps_db_pass']);
		    self::$_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);    
		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e->getMessage();
		}			
	}
	
	public function getRow($sql, $params = array()) {
		
		try {
		    $stmt = self::$_conn->prepare($sql);
		    $stmt->execute($params);
		 
		    while($row = $stmt->fetch()) {
		       return $row;
		    }
		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e->getMessage();
		}		
	}
	
	public function getRows($sql, $params = array()) {
		
		try {
		    $stmt = self::$_conn->prepare($sql);
		    $stmt->execute($params);
		 
		  	while($row = $stmt->fetchAll()) {
		       return $row;
		    }
		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e->getMessage();
		}		
	}	
		
}
?>