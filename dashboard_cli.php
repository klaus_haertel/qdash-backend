#!/usr/bin/php
<?php

include_once('classes/Startup.php');

print_r($config);

$slots = Utilities::getSlots('10min', $config['customer_id']);

$widgets = array();

print_r($slots);

foreach($slots as $widget) {
	$class = $widget->widget_class;	
	$widgets[] = new $class($widget->slot_id, $widget->parameter);
}

foreach ($widgets as $widget) {
	//registering thresholds
	//$widget->setThreshold($thresholds[$name]);
	
	//push data
	echo 'Pushing Widget Data for ' . get_class($widget) . '... ';
	$widget->push();
	echo "\n";

	//send alerts
	if ($config['send_alerts']) {
		if ($widget->hasCrossedThreshold()) {
			$widget->sendAlert();
		}
	}
}

/**
	TODO:
	'CostRevenueByService' => new CostRevenuePerService(''),
	
	DONE:
	'ACD_OB' => new ACD('174370', 'originate'),
	'ACD_IB' => new ACD('174371', 'answer'),	
	'ACDByVendor' => new ACDByVendor('176900')
	'PDD' => new PDD('176883'),
	'TotalASR' => new NetworkStats('159598', 'asr'),
	'TotalNER' => new NetworkStats('159599', 'ner')
	'VolumeONNETPerMonth' => new VolumePerMonth('159454','ONNET'),
	'VolumeInPerMonth' => new VolumePerMonth('159455','IN'),
	'VolumeOUTPerMonth' => new VolumePerMonth('159456','OUT'),
	'CallsPerDay' => new CallsPerDay('174366'),
	'TopVendorsOB' => new TopVendors('176867', 'originate'),
	'TopVendorsIB' => new TopVendors('176868', 'answer'),
	'ConnectionUtilizationOB' => new ConnectionUtilization('174356', 'originate'),
	'ConnectionUtilizationIB' => new ConnectionUtilization('174367', 'answer'),		
	'TopDestinations' => new TopDestinations('176861'),
	'TooShortCalls' => new UnusualCalls('176132', 'short'),
	'TooLongCalls' => new UnusualCalls('176134', 'long'),
	'FailedCalls' => new FailedCalls('174951'),
	'TopCustomers' => new TopCustomers('160091'),
	'InactiveCustomers' => new InactiveCustomers('167486'),
	'NewActiveCustomers' => new NewActiveCustomers('167318'),
	'OverdueInvoices' => new OverdueInvoices('167489','overdue'),
	'UnpaidInvoices' => new OverdueInvoices('167490','unpaid')
	'RevenuePerDay' => new CostRevenuePerDay('177732', 'Revenue'),
	'CostPerDay' => new CostRevenuePerDay('177733', 'Cost'),
	'RevenueCurrentMonth' => new RevenueCurrentMonth('168013','177732'),

	'CallsPerMonthThis' => new CallsPerMonth('158572', '158571', '158570'),
	'CallsPerMonth' => new CallsPerMonth('158572', '158571', '158570'),
	'RegisteredLocations' => new RegisteredLocations('171752', $connection_sip),




	'CallsPerDay' => array(),
	'TopCustomers' => array(),
	'VolumeONNETPerMonth' => array(),
	'VolumeInPerMonth' => array(),
	'VolumeOUTPerMonth' => array(),
	'TotalASR' => array(),
	'TotalNER' => array()	 
	'NewActiveCustomers' => array(),
	'InactiveCustomers' => array(),
	'OverdueInvoices' => array(),
	'UnpaidInvoices' => array(),
	'RevenueCurrentMonth' => array(),
	'RegisteredLocations' => array(),	
	'ConnectionUtilizationOB' => array(),
	'ConnectionUtilizationIB' => array(),
	'ACD_OB' => array(),
	'ACD_IB' => array(),
	'FailedCalls' => array(),
	'TooShortCalls' => array(),
	'TooLongCalls' => array(),
	'TopDestinations' => array(),
	'TopVendorsOB' => array(),
	'TopVendorsIB' => array(),
	'CallsPerMonth' => array(),
	'PDD' => array('direction' => '<', 'value' => 30),
	'ACDByVendor' => array(),
	'RevenuePerDay' => array(),
	'CostPerDay' => array(),
	'CostRevenueByService' => array(),
	'ActiveCalls' => array('direction' => '<', 'value' => 30),
*/

?>