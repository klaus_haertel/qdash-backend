<?php
//Widget specific

//NewActiveCustomers
$config['NAC_TIME_FRAME'] = 1; 

//InactiveCustomers
$config['IC_TIME_FRAME'] = 7; //no calls in 7 days

//RevenueCurrentMonth
$config['RCM_REVENUEGOAL'] = 1000; 

//FailedCalls
$config['FAILEDCALLS_TIME_FRAME'] = 600; //600 seconds, last 10 minutes

//UnusualCalls
$config['UC_TOO_SHORT'] = 10; //calls that are shorter than 10s
$config['UC_TOO_LONG'] = 7200; //calls that are longer than 2hrs

//ACDByVendor
$config['ACD_BY_VENDOR_TIME_FRAME'] = 3600; //60minutes
$config['PDD_RED_THESHOLD'] = 10000; //10000 ms = 10s
$config['PDD_YELLOW_THESHOLD'] = 4000; //4000 ms = 40s
$config['SHOW_INTERNAL_CNX'] = true;


//TopDestinations
$config['TOP_DESTINATIONS_TIME_FRAME'] = 3600;  //60 minutes

//TopVendors
$config['TOP_VENDORS_TIME_FRAME'] = 86400; //24 hrs


?>