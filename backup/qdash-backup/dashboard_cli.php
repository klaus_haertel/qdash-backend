#!/usr/bin/php
<?php

include_once('config/global.php');
include_once('config/widgets.php');

include_once('classes/Widget.php');
include_once('classes/PortaSwitchDB.php');

foreach (glob("Widgets/*.php") as $filename)
{
    include $filename;
}

//globals
Mandrill::setKey($config['MANDRILL_API_KEY']);
date_default_timezone_set($config['TZ']);
$testmode = $config['testmode'];

global $config;

$connection = new PortaSwitchDB();
$connection_sip = new PortaSwitchDB('sip');

$widgets = array(
	'ActiveCalls' => new ActiveCalls('158528', $connection),
	'CallsPerMonth' => new CallsPerMonth('158572', $connection, '158571', '158570'),
);

/**
	'RevenuePerDay' => new RevenuePerDay('167647', $connection),
	'RevenueCurrentMonth' => new RevenueCurrentMonth('168013','167647', $connection),
	'CallsPerDay' => new CallsPerDay('174366', $connection),
	'CallsPerMonthThis' => new CallsPerMonth('158572', $connection, '158571', '158570'),
	'TopCustomers' => new TopCustomers('160091', $connection),
	'VolumeONNETPerMonth' => new VolumePerMonth('159454','ONNET', $connection),
	'VolumeInPerMonth' => new VolumePerMonth('159455','IN', $connection),
	'VolumeOUTPerMonth' => new VolumePerMonth('159456','OUT', $connection),
	'TotalASR' => new TotalASR_NER('159598', 'asr' , $connection),
	'TotalNER' => new TotalASR_NER('159599', 'ner' , $connection)
	'NewActiveCustomers' => new NewActiveCustomers('167318', $connection),
	'InactiveCustomers' => new InactiveCustomers('167486', $connection),
	'OverdueInvoices' => new OverdueInvoices('167489','overdue', $connection),
	'UnpaidInvoices' => new OverdueInvoices('167490','unpaid', $connection)
	'RegisteredLocations' => new RegisteredLocations('171752', $connection_sip),
	'ConnectionUtilizationOB' => new ConnectionUtilization('174356', $connection, 'originate'),
	'ConnectionUtilizationIB' => new ConnectionUtilization('174367', $connection, 'answer'),		
	'ACD_OB' => new ACD('174370', $connection, 'originate'),
	'ACD_IB' => new ACD('174371', $connection, 'answer'),	
	'FailedCalls' => new FailedCalls('174951', $connection),
	'ACDByVendor' => new ACDByVendor('174970', $connection),


	'CallsPerDay' => array(),
	'TopCustomers' => array(),
	'VolumeONNETPerMonth' => array(),
	'VolumeInPerMonth' => array(),
	'VolumeOUTPerMonth' => array(),
	'TotalASR' => array(),
	'TotalNER' => array()	 
	'NewActiveCustomers' => array(),
	'InactiveCustomers' => array(),
	'OverdueInvoices' => array(),
	'UnpaidInvoices' => array(),
	'RevenuePerDay' => array(),
	'RevenueCurrentMonth' => array(),
	'RegisteredLocations' => array(),	
	'ConnectionUtilizationOB' => array(),
	'ConnectionUtilizationIB' => array(),
	'ACD_OB' => array(),
	'ACD_IB' => array(),
	'FailedCalls' => array(),
	'ACDByVendor' => array(),
*/

$thresholds = array(
	'ActiveCalls' => array('direction' => '<', 'value' => 30),
	'CallsPerMonth' => array(),
);

foreach ($widgets as $name => $widget) {
	//registering thresholds
	$widget->setThreshold($thresholds[$name]);
	
	//push data
	echo 'Pushing Widget Data for ' . $name . '... ';
	$widget->push();
	echo "\n";

	//send alerts
	if ($config['SEND_ALERTS']) {
		if ($widget->hasCrossedThreshold()) {
			$widget->sendAlert();
		}
	}
}


?>