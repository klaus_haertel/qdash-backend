<?php

class NewActiveCustomers extends Widget
{
	public function __construct($nbWidget, $connection)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$now_date = $connection->makeTime();
		
		if ($this->_testmode) {
			$past_date = $connection->makeTime(0,-6,0);
		}
		else {
			$past_date = $connection->makeTime(0,0,'-'.$config['NAC_TIME_FRAME']);
		}
		
		$sql = "select count(distinct C.name) from Customers C, CDR_Accounts A 
				where C.i_customer = A.i_customer and 
				C.i_env = ".$config['I_ENV']." and  
				C.bill_status = 'O' 
				and A.i_service = 3 and 
				C.creation_date between '".$past_date."' 
				and '".$now_date."'
				";
		
		echo $sql;
		
		$result = $connection->query($sql);
		
		print_r($result);
		
		while ($row = $result->fetch_row()) {
			$gaugeValue = $row['0'];
		}
		
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}

?>