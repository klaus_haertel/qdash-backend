<?php

class CallsPerMonth extends Widget
{
	private $_last = null;
	private $_last2 = null;
	private $_isMaster = false;
	private $_firstRun = false;
	
	private $_connection = null;
	
	public function __construct($nbWidget, $connection, $WidgetLast = 0, $Widget2Last = 0)
	{
		parent::__construct($nbWidget);
		
		$this->_connection = $connection;
		
		$this->_isMaster = ( ($WidgetLast <> 0) && ($Widget2Last <> 0) ); 
		
		//only perform this if we are the 'master' widget
		if ($this->_isMaster) {
			//0 = this month
			$gaugeValue = $this->performQuery(0);
	
			$this->setData(
				array(
					'value' => $gaugeValue
				)
			);
			
			//check if there is value for last month, if not this means this is the first run
			$this->_last = new CallsPerMonth($WidgetLast, $connection);
			
			//TO BE DONE
			$value = $this->_last->pull('last', 1); 
			
			echo($value);
						
			if ($value == null) {
				
				//if value is 0, this must be the first run
				$this->_firstRun = true;
				
				
				//get last month and month before
				//-1 = last month
				$gaugeValue = $this->performQuery(-1);
	
				$this->_last->setData(
					array(
						'value' => $gaugeValue
					)
				);
				
				//2nd last month
				$this->_last2 = new CallsPerMonth($Widget2Last, $connection);
				
				//-2 = 2nd last month
				$gaugeValue = $this->performQuery(-2);
	
				$this->_last2->setData(
					array(
						'value' => $gaugeValue
					)
				);
			}
			
		}
	}
	
	public function push() {
		parent::push();
		
		//only push the other two months if this is master and we are on the first run
		if ($this->_isMaster && $this->_firstRun) {
			$this->_last->push();
			$this->_last2->push();
		}		
	}
	
	public function performQuery($monthModifier) {
		global $config;		
				
		$thisyear = date("Y");
		$thismonth = date("m", mktime(0, 0, 0, date("m") + intval($monthModifier), date("d"),   date("Y")));
		$lastday = date("d", mktime(0,0,0,date("m")+ intval($monthModifier) +1 ,0 , $thisyear));
		
		$sql = "SELECT Count(*) FROM CDR_Vendors where `i_env` = '". $config['I_ENV'] ."' and `bill_time` between '".$thisyear."-".$thismonth."-01' and '".$thisyear."-".$thismonth."-".$lastday."' and `i_service` = '3'";
		
		$result = $this->_connection->query($sql);
				
		while ($row = $result->fetch_row()) {
			$gaugeValue = $row['0'];
		}
		
		return $gaugeValue;
	}
	
}
?>