<?php

class RevenuePerDay extends Widget
{
	public function __construct($nbWidget, $connection)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		if ($config['testmode']) {
			$yesterday = '2013-04-22 00:00:00';
			$today = '2013-04-23 00:00:00';			
		}
		else {
			$yesterday = date("Y-m-d H:i:s", mktime(0,0,0,date("m"), date("d") - 1, date("Y")));
			$today = date("Y-m-d H:i:s", mktime(0,0,0,date("m"), date("d"), date("Y")));
		}

		//get currencies and exchange rates for yesterday

		$currencies = array();
		
		$sql = "SELECT timestamp, iso_4217, base_iso_4217, base_units 
				FROM `porta-billing`.X_Rates 
				where i_env= ".$config['I_ENV']." 
				AND timestamp between '".$yesterday."' and '".$today."'";
		
		$result = $connection->query($sql);

		while ($row = $result->fetch_assoc()) {
			$currencies[$row['iso_4217']] = $row;
		}
		
		//get customer revenue (subscriptions mainly)
		
		/**
		i_service = 1 - Credits/Refunds
		i_service = 2 - Payments
		i_service = 11 - Cleanup
		i_service = 1 - Taxes
		*/
		
		$sql = "SELECT iso_4217, sum(charged_amount) as total FROM CDR_Customers cdr, customers c 
				where c.i_env = ".$config['I_ENV']."
				and (
					cdr.i_service != 1 and cdr.i_service != 11 and cdr.i_service != 2 and cdr.i_service != 12
					)
				and c.i_customer = cdr.i_customer
				and c.bill_status = 'O'
				and cdr.bill_time between '".$yesterday."' and '".$today."'
				group by iso_4217";

		$result = $connection->query($sql);

		$customer_revenue = 0;
		
		while ($row = $result->fetch_assoc()) {
			//get exchange rate from earlier
			$x_rate = $currencies[$row['iso_4217']]['base_units'];
			//get total by country
			$total = $row['total'];
				
			if (settype($x_rate, "float") && settype($total, "float")) {
				$revenue_per_currency =  $x_rate * $total;	
			}
			else $revenue_per_currency = 0;
			
			$customer_revenue = $customer_revenue + round($revenue_per_currency, 4);		
		}
		
		//get account revenue
		
		$account_revenue = 0;
		
		$sql = "SELECT iso_4217, sum(charged_amount) as total FROM CDR_Accounts cdr, customers c 
				where c.i_env = ".$config['I_ENV']."
				and (
					cdr.i_service != 1 and cdr.i_service != 11 and cdr.i_service != 2 and cdr.i_service != 12
					)
				and c.i_customer = cdr.i_customer
				and c.bill_status = 'O'
				and cdr.bill_time between '".$yesterday."' and '".$today."'
				group by iso_4217";

		$result = $connection->query($sql);

		while ($row = $result->fetch_assoc()) {
			//get exchange rate from earlier
			  
			if ($currencies[$row['iso_4217']] == '') {
				//if there is no entry in currencies array, this means we're dealing with base currency
				$x_rate = 1;
			}
			else $x_rate = $currencies[$row['iso_4217']]['base_units'];
			
			//get total by country
			$total = $row['total'];

			if (settype($x_rate, "float") && settype($total, "float")) {
				$revenue_per_currency =  $x_rate * $total;	
			}
			else $revenue_per_currency = 0;
			
			$account_revenue = $account_revenue + round($revenue_per_currency, 4);	
		}

		$total_revene = bcadd($customer_revenue, $account_revenue, 4);

		$this->setData(
			array(
				'value' => $total_revene
			)
		);
	}
}
?>