<?php

class NetworkStats extends Widget
{
	private $_connection = null;
	
	public function __construct($nbWidget, $type, $connection)
	{
		parent::__construct($nbWidget);

		$this->_data = array();
		$this->_connection = $connection;


		$timestamp = mktime(0,0,0,$thismonth,$thisday,$thisyear);		
		
		/**
		All calls that are voice calls, not internal, between X and Y (1hr), billable + failed
		*/
		//billable				
		$attempts = intval($this->performQuery('<>', '0', 'success'));
		//failed
		$attempts = $attempts + intval($this->performQuery('<>', '0', 'failed'));
		 		
		/**
		Successfull calls: all voice calls, not internal, between X and Y and cause code = 16
		*/
		$success = $this->performQuery('=', '16','success');

		//echo "Success: " .$success; 		

		/**
		User Busy calls: all voice calls, not internal, between X and Y and cause code = 17
		*/
		$busy = $this->performQuery('=', '17', 'failed');

		//echo "Busy: " .$busy; 		
				
		/**
		All Ring No Answer that are voice calls, not internal, between X and Y (1hr) and cause code = 18
		*/				
		$noanswer = $this->performQuery('=', '18', 'failed');
		$noansweruser = $this->performQuery('=', '19', 'failed'); 
		
		//echo "No Answer: " .$noanswer . " " .$noansweruser; 		

		/**
		All rejects that are voice calls, not internal, between X and Y (1hr) and cause code = 21
		*/				
		$rejects = $this->performQuery('=', '21', 'failed');

		if (intval($attempts) == 0) {
			// no calls made, set ASR to 100%
			$asr = 1;
			$ner = 1;
		}
		else {
			$asr = intval($success) / intval($attempts);
			$ner =  (intval($success) + intval($noanswer) + intval($noansweruser) + intval($busy) + intval($rejects)) / intval($attempts);		
		}
		
		if ($type == 'asr') {
			$this->setData(array('value' => $asr));
			
		}
		else if ($type == 'ner') {
			$this->setData(array('value' => $ner));
		}
	}


	private function performQuery($op, $cause, $table) {
		/**
		Builds the proper query and executes it
		*/
		global $config;
		
		$thisyear = date("Y");
		$thismonth = date("m");
		$thisday = date("d");

		$now_date = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), $thismonth,$thisday,$thisyear));
		$last_hour_date = date("Y-m-d H:i:s", mktime(date("H")-1, date("i"), date("s"), $thismonth,$thisday,$thisyear));

		// in testmode we need more data
		
		if ($config['testmode']) {
			$now_date = "2013-04-30 00:00:00";
			$last_hour_date = "2013-04-01 00:00:00";
		}


		if ($table == 'success') {
			$sql = 
			"SELECT Count(*) FROM CDR_Vendors where 
			`i_env` = '". $config['I_ENV'] ."' and 
			`bill_time` between '".$last_hour_date."' and '".$now_date."' and 
			`i_service` = '3' and ";
			
			//`i_connection` <> '".\Ducksboard\PortaSwitchDB::INTERNAL_CNX_SIP."' and 
			//`i_connection` <> '".\Ducksboard\PortaSwitchDB::INTERNAL_CNX_UM."' and ";
		
			$sql = $sql . "`disconnect_cause` ".$op." '".$cause."'";
			
		}
		else {
			//$table = failure
			$sql = 
				"SELECT Count(*) FROM CDR_Vendors_Failed where 
				`i_env` = '". $config['I_ENV'] ."' and 
				`bill_time` between '".$last_hour_date."' and '".$now_date."' and 
				`i_service` = '3' and ";
						
				$sql = $sql . "`disconnect_cause` ".$op." '".$cause."'";
		}

		$result = $this->_connection->query($sql);
						
		while ($row = $result->fetch_row()) {
			$nr_calls = $row['0'];
		}
		
		return $nr_calls;
		
	}

}

?>
