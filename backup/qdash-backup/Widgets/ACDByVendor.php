<?php

class ACDByVendor extends Widget
{
	
	public function __construct($nbWidget, $connection)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		if ($this->_testmode) {
			//more data in testmode
			$now_date = $connection->makeTime();
			$past_date = "2000-01-01";	
		}
		else {
			$now_date = $connection->makeTime();
			$past_date = $connection->makeTime(0,0,0,0,'-'.$config['ACD_BY_VENDOR_TIME_FRAME']);
		}
		
		echo($now_date . $past_date);
		
		$this->_board = array();
								
		$sql = "
				SELECT 
				    V.name as VName,
				    avg(charged_quantity) as ACD,
				    count(CDR.id) as Calls,
				    sum(charged_quantity) as Total
				FROM
				    CDR_Vendors CDR
				        LEFT JOIN
				    Connections C ON C.i_connection = CDR.i_connection
				        LEFT JOIN
				    Vendors V ON V.i_vendor = C.i_vendor
				WHERE
				    CDR.i_env = ".$config['I_ENV']." 
				    and charged_quantity > 0
				    and i_service = 3
				    and call_origin = 'originate'
				    and bill_time between '".$past_date."' and '".$now_date."'
				GROUP BY C.i_vendor		";
		
		$result = $connection->query($sql);
		
		while ($row = $result->fetch_assoc()) {
			$this->_board[] = array('name' => $row['VName'], 'values' => array($row['Calls'], gmdate("H:i:s",intval($row['Total'])), gmdate("H:i:s", intval($row['ACD']))));
		}
		
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>