<?php

class OverdueInvoices extends Widget
{
	public function __construct($nbWidget, $mode, $connection)
	{
		parent::__construct($nbWidget);
		
		global $config;
			
		if ($mode == 'overdue') {
			//invoice status = 4 (overdue)
			$sql = "select count(distinct C.name) 
				from Customers C, Invoices I 
				where C.i_customer = I.i_customer and 
					C.i_env = '".$config['I_ENV']."' and  
					C.bill_status = 'O' and 
					I.i_invoice_status = 4";
			
		}		
		else {
			//bill status = 2 (unpaid)
			$sql = "select count(distinct C.name) 
				from Customers C, Invoices I 
				where C.i_customer = I.i_customer and 
					C.i_env = '".$config['I_ENV']."' and  
					C.bill_status = 'O' and 
					I.i_invoice_status = 2";
		}
		$result = $connection->query($sql);
		
		while ($row = $result->fetch_row()) {
			$gaugeValue = $row['0'];
		}
		
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}

?>