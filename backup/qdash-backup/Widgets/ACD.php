<?php

class ACD extends Widget
{
	private $_connection = null;
	
	public function __construct($nbWidget, $connection, $mode)
	{
		parent::__construct($nbWidget);

		$this->_data = array();
		$this->_connection = $connection;

		global $config;
		
		$thisyear = date("Y");
		$thismonth = date("m");
		$thisday = date("d");

		$now_date = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), $thismonth,$thisday,$thisyear));
		$last_hour_date = date("Y-m-d H:i:s", mktime(date("H")-1, date("i"), date("s"), $thismonth,$thisday,$thisyear));

		// in testmode we need more data
		
		if ($config['testmode']) {
			$now_date = "2013-04-30 00:00:00";
			$last_hour_date = "2013-04-01 00:00:00";
		}


		$timestamp = mktime(0,0,0,$thismonth,$thisday,$thisyear);		
		
		$sql = "
				SELECT 
				    AVG(charged_quantity) as ACD
				FROM
				    CDR_Vendors CDR
				        LEFT JOIN
				    Connections C ON C.i_connection = CDR.i_connection
				WHERE
				    CDR.i_env = ".$config['I_ENV']." 
				    	and charged_quantity > 0
				        and i_service = 3
				        and bill_time between '".$last_hour_date."' and '".$now_date."'
				        and call_origin = '".$mode."'
				        and C.i_connection <> ".$config['INTERNAL_CNX_SIP']." 
						and C.i_connection <> ".$config['INTERNAL_CNX_UM']."
		";

		$result = $this->_connection->query($sql);

		while ($row = $result->fetch_assoc()) {
			if ($row['ACD'] <> '') $acd = intval($row['ACD']);
				else $acd = 0;
		}

		$this->setData(array('value' => $acd));
	}
}

?>
