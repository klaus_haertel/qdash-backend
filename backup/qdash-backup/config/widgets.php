<?php
//Widget specific

//NewActiveCustomers
$config['NAC_TIME_FRAME'] = 1; 

//InactiveCustomers
$config['IC_TIME_FRAME'] = 7; //no calls in 7 days

//RevenueCurrentMonth
$config['RCM_REVENUEGOAL'] = 1000; 

//FailedCalls
$config['FAILEDCALLS_TIME_FRAME'] = 600; //600 seconds, last 10 minutes

//ACDByVendor
$config['ACD_BY_VENDOR_TIME_FRAME'] = 60; //60 minutes, last 1hr

?>