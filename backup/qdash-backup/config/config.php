<?php
//Global
$config['testmode'] = false;
$config['TZ'] = 'America/Los_Angeles';

//Dashboard API
$config['API_KEY'] = 'RoNpq1o68B93B0aKeBS6tCvbj7Nahaxo9tpK5eV8Xim99kI6HU';

//Mandrill API
$config['MAIL_API_KEY'] = 'o1LWCdPS9HQbf3MPU6KxgQ';
$config['SMTP_USER'] = 'klaus@signalq.ca';
$config['SMTP_PORT'] = 587;
$config['SMTP_HOST'] = 'mandrillapp.com';

//Alerts
$config['MANDRILL_API_KEY'] = 'o1LWCdPS9HQbf3MPU6KxgQ'; 
$config['SEND_ALERTS'] = false;
$config['FROM_EMAIL'] = 'klaus@signalq.ca';
$config['FROM_NAME'] = 'Klaus Haertel';
$config['TO_EMAIL'] = 'klaus.haertel@gmail.com';
$config['TO_NAME'] = 'Klaus Haertel';
$config['ALERT_SUBJ'] = 'qDash Alert: ';

//DB Connection, Slave
$config['DB_USER'] = 'reports';
$config['DB_PASS'] = 'kh41hkph';
$config['DB_HOST'] = '127.0.0.1';
$config['DB_NAME'] = 'porta-billing';
$config['DB_PORT'] = '3307';

//DB Connection, Master
$config['DB_USER_MS'] = 'reports';
$config['DB_PASS_MS'] = 'kh41hkph';
$config['DB_HOST_MS'] = 'porta-billing-master';
$config['DB_NAME_MS'] = 'porta-sip';
$config['DB_PORT_MS'] = '3307';


//PortaSwitch specific
$config['INTERNAL_CNX_SIP']= '20';
$config['INTERNAL_CNX_UM'] = '21';
$config['INTERNAL_VENDOR'] = '10';
$config['I_ENV'] = '6';


//Widget specific

//NewActiveCustomers
$config['NAC_TIME_FRAME'] = 1; 

//InactiveCustomers
$config['IC_TIME_FRAME'] = 7; //no calls in 7 days

//RevenueCurrentMonth
$config['RCM_REVENUEGOAL'] = 1000; 


?>