<?php
require_once('classes/Mandrill.php');
require_once('classes/DB.php');

abstract class Widget 
{	
	/**
	 * @var resource The cURL handle that will be used
	 */
	private $_curlHandle = null;
	private $_curlPull = null;
	
	/**
	* The Mandrill configuration for Alerts
	*/
	private $_apikey = 'o1LWCdPS9HQbf3MPU6KxgQ';
	private $_fromname = 'qDash';
	private $_fromemail = 'qdash@signalq.ca';
	private $_alertsubject = 'qDash Alert: ';

	/**
	 * @var array The data that should be sent
	 */
	private $_data = array();
	/**
	 * @var array The alert threshold to use
	 */
	private $_thresholds = array();

	private $_nbWidget = null;
	private $_db = null;

	public $_testmode = false;


	public function __construct($nbWidget, $dbtype = 'slave')
	{	
		global $config;
		
		//the ducksboard widget number
		$this->_nbWidget = $nbWidget;
		
		//can be 'master' or 'slave' - most data is from slave DB		
		$this->_db = new DB($dbtype);

		$this->_curlHandle = curl_init(
			'https://push.ducksboard.com/values/' . $nbWidget . '/'
		);

			
		curl_setopt(
			$this->_curlHandle,
			CURLOPT_USERPWD,
			$config['qdash_apikey'] . ':ignored'			
		);
		
		$this->_testmode = $config['testmode'];
	}

	public function doQuery($sql, $params = array()) {
		return $this->_db->getRow($sql, $params);
	}
	
	public function doQueryAll($sql, $params = array()) {
		return $this->_db->getRows($sql, $params);
	}	
	
	public function setThreshold(array $thresholds) {
		//metric direction $value
		//Ex: ASR   52 ($metric) < ($thresholds[0]->direction)  65 ($thresholds[0]->value)
		$this->_thresholds = $thresholds;
	}
	
	public function sendAlert() {

		global $config;
		
		Mandrill::setKey($this->_apikey);
		
		$to_arr = array();
		array_push($to_arr, array('email' => $config['TO_EMAIL'], 'name' => $config['TO_NAME']));
		
		//class is \\Dashboard\\Widget\\NameOfWidget
		$class = explode("\\", get_class($this));
				
		$subj = $this->_alertsubject.$class[2];
		
		$text = 'Current value: '. $this->_data['value'] ."\n" . 'Threshold value: ' . $this->_thresholds['value'];
		
		$email_arr = array('type' => 'messages', 
						'call' => 'send', 
						'message' => array('text' => $text,
											'subject' => $subj,
											'from_email' => $this->_fromemail,
											'from_name' => $this->fromname,
											'to' => $to_arr
							)
				);
		$json = json_encode($email_arr);	
		$ret = Mandrill::call((array) json_decode($json));
	}


	public function hasCrossedThreshold() {
		
		//get the operator
		$op = $this->_thresholds['direction'];
		
		if ($op == '<=') {
			//less than or equal
			$crossed = ($this->_data['value'] <= $this->_thresholds['value']);
		}
		else {
			//greater than or equal
			$crossed = ($this->_data['value'] >= $this->_thresholds['value']);
		}
		return ($crossed==1);
	}

	public function setData(array $data)
	{
		$this->_data = $data;
	}

	public function push()
	{
		curl_setopt(
			$this->_curlHandle,
			CURLOPT_POSTFIELDS,
			json_encode($this->_data)
		);
		print(json_encode($this->_data));
		curl_exec($this->_curlHandle);
	}

	public function pull($method, $parameter) 
	{
		global $config;
		
		$url = 'https://pull.ducksboard.com/values/' . $this->_nbWidget . '/' . $method;
		
		if ($method == 'since') {
			$gets = array('seconds' => $parameter);
		}
		else {
			$gets = array('count' => $parameter);
		}
		
		$options = array(CURLOPT_USERPWD => $config['qdash_apikey'] . ':ignored');
		$result = Utilities::curl_get($url, $gets, $options);
		 
		return json_decode($result,true);		
	}


	public function __destruct()
	{
		curl_close($this->_curlHandle);
	}
}
?>