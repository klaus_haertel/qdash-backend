<?php

class NetworkStats extends Widget
{
	private $_connection = null;
	
	public function __construct($nbWidget, $type)
	{
		parent::__construct($nbWidget);

		$this->_data = array();

		/**
		All calls that are voice calls, not internal, between X and Y (1hr), billable + failed
		*/
		//billable				
		$attempts = intval($this->performQuery('<>', '0', 'success'));
		
		//failed
		$attempts = $attempts + intval($this->performQuery('<>', '0', 'failed'));
		 
		if (intval($attempts) == 0) {
			// no calls made, set ASR to 100%
			$asr = 1;
			$ner = 1;
		}
		else {
		 		 		
			/**
			Successfull calls: all voice calls, not internal, between X and Y and cause code = 16
			*/
			$success = $this->performQuery('=', '16','success');
	
			//these numbers are not needed for ASR calculation
			switch ($type) {
				/**
				Type can be 
				asr: answer-seizure ratio (successful/attempts)
				ner: network-efficiency ratio (success + failed calls due to callee issue/attempts)
				clr: call-loss ratio (calls failed due to network issue/attempts)
				*/
				case 'asr': 
					$asr = intval($success) / intval($attempts);
					$this->setData(array('value' => $asr));
					break;
				
				case 'ner':
					/**
					User Busy calls: all voice calls, not internal, between X and Y and cause code = 17
					*/
					$busy = $this->performQuery('=', '17', 'failed');
			
					//echo "Busy: " .$busy; 		
							
					/**
					All Ring No Answer that are voice calls, not internal, between X and Y (1hr) and cause code = 18
					*/				
					$noanswer = $this->performQuery('=', '18', 'failed');
					$noansweruser = $this->performQuery('=', '19', 'failed'); 
					
					//echo "No Answer: " .$noanswer . " " .$noansweruser; 		
			
					/**
					All rejects that are voice calls, not internal, between X and Y (1hr) and cause code = 21
					*/				
					$rejects = $this->performQuery('=', '21', 'failed');
					
					$ner =  (intval($success) + intval($noanswer) + intval($noansweruser) + intval($busy) + intval($rejects)) / intval($attempts);
					$this->setData(array('value' => $ner));

					break;
					
				case 'clr':
					/**
					All calls that failed due to a network error (Disconnect Reason Class = 5)
					*/
					$network_issue = $this->performQuery('=','5','failed+disconnect_reasons');
					
					//echo($network_issue." ".$attempts);
					
					$clr = intval($network_issue)/ intval($attempts);
					$this->setData(array('value' => $clr));
					break;
					
			}	
		
					
		}
		
	}


	private function performQuery($op, $cause, $table) {
		/**
		Builds the proper query and executes it
		*/
		global $config;
		
		$thisyear = date("Y");
		$thismonth = date("m");
		$thisday = date("d");

		// in testmode we need more data		
		if ($this->_testmode) {
			$now_date = "2013-06-30 00:00:00";
			$last_hour_date = "2013-06-01 00:00:00";
		}
		else {
			$now_date = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), $thismonth,$thisday,$thisyear));
			$last_hour_date = date("Y-m-d H:i:s", mktime(date("H")-1, date("i"), date("s"), $thismonth,$thisday,$thisyear));			
		}

		switch ($table) {
			case 'success':
				$extra_clause = '';
				$tb_name = 'CDR_Vendors';
				$disonnect = "disconnect_cause ".$op." :cause";
						
				//`i_connection` <> '".\Ducksboard\PortaSwitchDB::INTERNAL_CNX_SIP."' and 
				//`i_connection` <> '".\Ducksboard\PortaSwitchDB::INTERNAL_CNX_UM."' and ";				
				break;
		
			case 'failed': 
				$extra_clause = '';
				$tb_name = 'CDR_Vendors_Failed';
				$disonnect = "disconnect_cause ".$op." :cause";
				break;
			default:
				//table = failure + disconnect reasons
				$tb_name = 'CDR_Vendors_Failed VF, Disconnect_Reasons DR';
				$extra_clause = 'VF.';
				$disonnect = "VF.disconnect_cause = DR.disconnect_cause
						  and DR.i_class ".$op." :cause";
		}

		$sql = "SELECT Count(*) FROM
				".$tb_name." 
				WHERE
				".$extra_clause."i_env = :i_env
				and bill_time between :last_hour_date and :now_date 
				and i_service = 3
				and ".$disonnect."";

		$params = array('i_env' => $config['ps_env'],
				'last_hour_date' => $last_hour_date,
				'now_date' => $now_date,
				'cause' => $cause,
		);
		$result = parent::doQuery($sql, $params);
						
		$nr_calls = $result[0];
		
		return $nr_calls;
	}

}

?>
