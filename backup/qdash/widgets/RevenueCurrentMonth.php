<?php

class RevenueCurrentMonth extends Widget
{
	public function __construct($nbWidget, $revDayWidget)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		if ($config['testmode']) {
			$start_of_month = '2013-04-22 00:00:00';
			$today = '2013-04-23 00:00:00';			
		}
		else {
			$start_of_month = date("Y-m-d H:i:s", mktime(0,0,0,date("m"), 1, date("Y")));
			$today = date("Y-m-d H:i:s", mktime(0,0,0,date("m"), date("d"), date("Y")));
		}
		
		//pull revenue data from RevenuePerDay widget
		
		// seconds between now and 1st of the month
		$since = mktime(date("H"),date("i"),date("s"),date("m"), date("d"), date("Y")) - mktime(0,0,0,date("m"), 1, date("Y")); 
		
		$daily_revenue = new CostRevenuePerDay($revDayWidget);
		$revenues = $daily_revenue->pull('since', $since);
		
		//revenues come back as array 
		
		$monthly_revenue = 0;
		$this_time = '';
		$prev_time = '';		
		
		foreach ($revenues['data'] as $revenue) {
		 	$this_time = date("Y-m-d", $revenue['timestamp']);
		 	
		 	//discard entries for the same day (it's the same value)
		 	if ($this_time <> $prev_time) {
			 	//new day
			 	$monthly_revenue = $monthly_revenue + $revenue['value']; 
			 	$prev_time = $this_time;
		 	}
		}
		
		//push totals		
		$this->setData(
			array(
				'value' => array('current' => round($monthly_revenue,2), 'max' => $config['RCM_REVENUEGOAL'])
			)
		);
		
		
	}
}
?>