<?php

//namespace Dashboard\Widget;

class PDD extends Widget
{
	public function __construct($nbWidget)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$sql = "
				Select 
				    avg(setup_time) as PDD
				from
				    CDR_Vendors CDR
				        JOIN
				    Vendors V ON CDR.i_vendor = V.i_vendor
				where
				    CDR.i_env = :i_env 
				    and CDR.i_service = 3
				    and CDR.charged_quantity <> 0
				    and CDR.i_vendor <> :internal_vendor		
		";
		
		$params = array('i_env' => $config['ps_env'],
						'internal_vendor' => $config['ps_internal_vendor'],
		);
		
		$result = parent::doQuery($sql, $params);		
		$gaugeValue = $result[0];
		
		/**
		if ($this->_testmode) {
			$gaugeValue = rand(37, 198);
		}
		*/
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}
?>