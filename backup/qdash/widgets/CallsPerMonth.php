<?php

class CallsPerMonth extends Widget
{
	private $_last = null;
	private $_last2 = null;
	private $_isMaster = false;
	private $_firstRun = false;
	
	private $_connection = null;
	
	public function __construct($nbWidget, $WidgetLast = 0, $Widget2Last = 0)
	{
		parent::__construct($nbWidget);
		
		$this->_isMaster = ( ($WidgetLast <> 0) && ($Widget2Last <> 0) ); 
		
		//only perform this if we are the 'master' widget
		if ($this->_isMaster) {
			//0 = this month
			$gaugeValue = $this->performQuery(0);
	
			$this->setData(
				array(
					'value' => $gaugeValue
				)
			);
			
			//check if there is value for last month, if not this means this is the first run
			$this->_last = new CallsPerMonth($WidgetLast);
			
			//value of last month
			$value = $this->_last->pull('last', 1); 
			
			//test whether there is data last month
			if( !is_array($value) || !is_array($value['data']) 
								|| !is_array($value['data'][0]) 
								|| !($value['data'][0]['value'] > 0) ) {

				//if value is 0, this must be the first run
				$this->_firstRun = true;
				
				
				//get last month and month before
				//-1 = last month
				$gaugeValue = $this->performQuery(-1);
	
				$this->_last->setData(
					array(
						'value' => $gaugeValue
					)
				);
				
				//2nd last month
				$this->_last2 = new CallsPerMonth($Widget2Last);
				
				//-2 = 2nd last month
				$gaugeValue = $this->performQuery(-2);
	
				$this->_last2->setData(
					array(
						'value' => $gaugeValue
					)
				);

			}				
		}
	}
	
	public function push() {
		parent::push();
		
		//only push the other two months if this is master and we are on the first run
		if ($this->_isMaster && $this->_firstRun) {
			$this->_last->push();
			$this->_last2->push();
		}		
	}
	
	public function performQuery($monthModifier) {
		global $config;		
				
		$thisyear = date("Y");
		$thismonth = date("m", mktime(0, 0, 0, date("m") + intval($monthModifier), date("d"),   date("Y")));
		$lastday = date("d", mktime(0,0,0,date("m")+ intval($monthModifier) +1 ,0 , $thisyear));
		
		$first_of_month = $thisyear."-".$thismonth."-01";
		$last_of_month = $thisyear."-".$thismonth."-".$lastday;
		
		$sql = "
				SELECT Count(*) FROM CDR_Vendors 
				WHERE i_env = :i_env
				AND bill_time between :first_of_month and :last_of_month 
				and i_service = '3'";
		
		$params = array('i_env' => $config['ps_env'],
						'first_of_month' => $first_of_month,
						'last_of_month' => $last_of_month,
		);
		
		$result = parent::doQuery($sql, $params);
				
		$gaugeValue = $row[0];
		
		return $gaugeValue;
	}
}
?>