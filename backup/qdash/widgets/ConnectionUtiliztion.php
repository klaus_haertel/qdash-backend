<?php

class ConnectionUtilization extends Widget
{
	
	public function __construct($nbWidget, $mode)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$this->_board = array();
								
		$sql = "SELECT
					name, 
					description,
					capacity,
				    (count(i_active_callleg) / capacity) * 100 as 'Utilization'
				FROM
				    Connections C
				        Left join
				    Active_Calls A ON A.i_connection = C.i_connection,
				    Vendors V
				WHERE
				    C.i_env = :i_env
				    and C.call_origin = :mode
				    and V.i_vendor = C.i_vendor
				GROUP BY C.i_connection
				ORDER BY Utilization DESC";
		
		$params = array('i_env' => $config['ps_env'], 'mode' => $mode);
		
		$result = parent::doQuery($sql, $params);
			
		if(!is_null($result)) {
			if ($result['Utilization'] > 90) $status = "red";
			elseif ($result['Utilization'] > 75) $status = "yellow";
			elseif ($result['Utilization'] > 0) $status = "green";
			else $status = "gray";	
			$this->_board[] = array('name' => $result['name']." ".$result['description'], 
									'values' => array($result['capacity'], intval($result['Utilization'])), 
									'status' => $status);
		}
		
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>