<?php

class VolumeByCustomer extends Widget
{

	private $_connection = null;
	
	public function __construct($nbWidget, $i_customer)
	{
		parent::__construct($nbWidget);

		global $config;
				
		$this->_data = array();

		$thisyear = date("Y");
		$thismonth = date("m");
		$thisday = date("d");
		
		$thishour = date("H");
		$lasthour = date("H")-1;
		$thismin = date("i");

		$start = $thisyear."-".$thismonth."-".$thisday." ".$lasthour.":".":00:00"; 
		$end = $thisyear."-".$thismonth."-".$thisday." ".$thishour.":00:00";

		$sql = "SELECT SUM(A.charged_quantity)/60 FROM CDR_Accounts A
			WHERE A.i_customer = :i_customer 
			AND A.i_env = :i_env 
			AND bill_time between :start and :end 
			AND i_service = '3'";
		
		$params = array('i_env' => $config['ps_env'],
						'start' => $start,
						'end' => $end,
						'i_customer' => $i_customer,
		);

		$result = parent::doQuery($sql, $params);
		
		$volume = $result[0];

		$timestamp = mktime($thishour,0,0, $thismonth , $thisday, $thisyear);		

		$this->_data[] = array('timestamp' => $timestamp, 'value' => intval($volume));
		
		$this->setData($this->_data);
	}
}
?>