<?php

class InactiveCustomers extends Widget
{
	public function __construct($nbWidget)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$now_date = Utilities::makeTime();
		$past_date = Utilities::makeTime(0,0,'-'.$config['IC_TIME_FRAME']);
		
		$sql = "SELECT count(*)
				FROM Customers c
				WHERE NOT EXISTS (
					SELECT 1
					FROM CDR_Accounts cdr USE INDEX (env_cust_srv_time)
					WHERE cdr.i_env = c.i_env
					AND cdr.i_customer = c.i_customer
					AND cdr.i_service = 3
					AND cdr.bill_time BETWEEN :past_date AND :now_date
				)
				AND c.bill_status = 'O'
				AND c.i_env = :i_env";
		
		$params = array('i_env' => $config['ps_env'],
						'past_date' => $past_date,
						'now_date' => $now_date,
		);
		
		$result = parent::doQuery($sql, $params);
		
		if(!is_null($result)) {
			$gaugeValue = $result[0];
		}
		
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}

?>