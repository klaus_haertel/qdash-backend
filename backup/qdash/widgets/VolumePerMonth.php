<?php

class VolumePerMonth extends Widget
{

	private $_connection = null;
	
	public function __construct($nbWidget, $mode)
	{
		parent::__construct($nbWidget);
		/**
		mode = {ONNET, IN, OUT}
		*/
				
		$this->_data = array();

		if ($mode == 'ONNET') {
			$volume = $this->performQuery('ONNETSIP') + $this->performQuery('ONNETUM'); 
		}
		else {
			$volume = $this->performQuery($mode);
		}

		$timestamp = mktime(0,0,0, date("m") , date("d"), date("Y"));		
		$this->_data[] = array('timestamp' => $timestamp, 'value' => intval($volume));
		$this->setData($this->_data);

	}
	
	private function performQuery($mode) {
		
		/**
		mode = {ONNETSIP, ONNETUM, IN, OUT}
		*/
		
		global $config;
		
		if ($this->_testmode) {
			$thisyear = '2013';
			$thismonth = '4';
			$thisday = '19';
		}
		else {
			$thisyear = date("Y");
			$thismonth = date("m");
			$thisday = date("d") - 1;
		}

		$thishour = date("H");
		$lasthour = date("H")-1;
		$thismin = date("i");
		
		$start = $thisyear."-".$thismonth."-".$thisday." ".$lasthour.":".":00:00"; 
		$end = $thisyear."-".$thismonth."-".$thisday." ".$thishour.":00:00";
				
		switch ($mode) {
			case 'ONNETSIP':
				$extra = "WHERE";
				$filter = "i_connection = ". $config['ps_internal_sip'];
				break;
			case 'ONNETUM':
				$extra = "WHERE";
				$filter = "i_connection = ".$config['ps_internal_um'];							
				break;
			case 'IN':
				$extra = ", Connections C WHERE 
							C.i_connection = V.i_connection
							AND ";
				$filter = "C.call_origin = 'answer'
							AND	V.i_vendor <> ". $config['ps_internal_vendor'];
			case 'OUT':
				$extra = ",Connections C WHERE 
						 C.i_connection = V.i_connection
						AND ";
				$filter = "C.call_origin = 'originiate'
							AND	V.i_vendor <> ". $config['ps_internal_vendor'];
				break;
		}
	
		$sql = "SELECT SUM(V.charged_quantity)/60 FROM CDR_Vendors V ". $extra ."
			V.i_env = :i_env 
			AND bill_time between :start and :end 
			AND i_service = '3' 
			AND ". $filter;
		
		$params = array('i_env' => $config['ps_env'],
						'start' => $start,
						'end' => $end,
		);

		$result = parent::doQuery($sql, $params);
		
		$volume = $result[0];
		
		if ($mode == 'ONNETSIP') {
			$volume = intval($volume) / 2;
		}
		return $volume;		
	}
}
?>