<?php

class NewActiveCustomers extends Widget
{
	public function __construct($nbWidget)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$now_date = Utilities::makeTime();
		
		if ($this->_testmode) {
			$past_date = Utilities::makeTime(0,-6,0);
		}
		else {
			$past_date = Utilities::makeTime(0,0,'-'.$config['NAC_TIME_FRAME']);
		}
		
		$sql = "select count(distinct C.name) from Customers C, CDR_Accounts A 
				where C.i_customer = A.i_customer and 
				C.i_env = :i_env and  
				C.bill_status = 'O' 
				and A.i_service = 3 and 
				C.creation_date between :past_date 
				and :now_date";
		
		$params = array('i_env' => $config['ps_env'],
						'past_date' => $past_date,
						'now_date' => $now_date,
		);
		
		$result = parent::doQuery($sql, $params);
		
		if(!is_null($result)) {
			$gaugeValue = $result[0];
		}
		
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}

?>