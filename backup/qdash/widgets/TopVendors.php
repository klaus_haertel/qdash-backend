<?php

class TopVendors extends Widget
{
	public function __construct($nbWidget, $mode)
	{
		parent::__construct($nbWidget);
		
		//$mode is either originate = outbound, or answer=inbound		
		global $config;
		
		$this->_board = array();
		
		if ($this->_testmode) {
			//more data in testmode
			$now_date = Utilities::makeTime();
			$past_date = "2000-01-01";	
		}
		else {
			$now_date = Utilities::makeTime();
			$past_date = Utilities::makeTime(0,0,0,0,0,'-'.$config['TOP_VENDORS_TIME_FRAME']);
		}

		$sql = "
				SELECT 
				    V.name VName, C.description CName, sum(charged_quantity) as TotalSecs
				FROM
				    CDR_Vendors CDR
				        JOIN
					Connections C ON CDR.i_connection = C.i_connection
						JOIN
				    Vendors V ON CDR.i_vendor = V.i_vendor
				WHERE
				    CDR.i_env = :i_env
					and i_service = 3
					and C.call_origin = :mode
					and bill_time between :past_date and :now_date
				GROUP BY C.i_connection, V.i_vendor
				Order by TotalSecs DESC
		";
			
		$params = array('i_env' => $config['ps_env'],
						'mode' => $mode,
						'past_date' => $past_date,
						'now_date' => $now_date,
		);	
			
		$result = parent::doQuery($sql, $params);
		
		if(!is_null($result)) {
			$this->_board[] = array('name' => $result['VName'].'-'.$result['CName'], 
									'values' => array(gmdate("H:i:s", intval($result['TotalSecs']))));
			$this->setData(
				array(
					'value' => array('board' => $this->_board)
				)
			);
		}						
	}
}
?>