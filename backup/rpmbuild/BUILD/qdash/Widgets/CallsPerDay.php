<?php

class CallsPerDay extends Widget
{
	public function __construct($nbWidget, $connection)
	{
		parent::__construct($nbWidget);
				
		global $config;
				
		$thisyear = date("Y");
		$thismonth = date("m");
		$thisday = date("d");
		
		$this->_data = array();

		$timestamp = mktime(0,0,0,$thismonth,$thisday,$thisyear);
						
		$sql = "SELECT Count(*) FROM CDR_Vendors where `i_env` = '". $config['I_ENV'] ."' and `bill_time` between '".$thisyear."-".$thismonth."-".$thisday." 00:00:00' and '".$thisyear."-".$thismonth."-".$thisday." 23:59:59' and `i_service` = '3'";
					
		$result = $connection->query($sql);
				
		while ($row = $result->fetch_row()) {
			$gaugeValue = $row['0'];
		}
		
		if ($config['testmode']) {
			$gaugeValue = rand(50000,95000);
		}
		
		$this->_data[] = array('timestamp' => $timestamp, 'value' => $gaugeValue);
		
		$this->setData($this->_data);

	}
}


?>