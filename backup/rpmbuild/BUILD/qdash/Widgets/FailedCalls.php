<?php

class FailedCalls extends Widget
{
	
	public function __construct($nbWidget, $connection)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		if ($this->_testmode) {
			//more data in testmode
			$now_date = $connection->makeTime();
			$past_date = "2000-01-01";	
		}
		else {
			$now_date = $connection->makeTime();
			$past_date = $connection->makeTime(0,0,0,0,0,'-'.$config['FAILEDCALLS_TIME_FRAME']);
		}
		
		$this->_board = array();
								
		$sql = "
				SELECT count(VF.disconnect_cause) as Hits, C.name as CustName, VF.account_id as Account, VF.CLI as CLI, 
				VF.CLD as CLD, VF.disconnect_cause as Cause, disconnect_text as Reason, V.name as Vendor, Countries.name as Country
				FROM  CDR_Vendors_Failed VF, Customers C, Disconnect_Reasons DR, Vendors V, Destinations D, Countries
					WHERE VF.i_env= ".$config['I_ENV']."
					AND VF.i_customer = C.i_customer
					AND VF.disconnect_cause = DR.disconnect_cause
					AND VF.i_vendor = V.i_vendor
					AND VF.i_dest = D.i_dest
					AND D.iso_3166_1_a2 = Countries.iso_3166_1_a2
					AND bill_time between '".$past_date."' AND '".$now_date."'
					GROUP BY C.name, VF.account_id, VF.disconnect_cause
					Order by Hits DESC		
		";
		
		$result = $connection->query($sql);
		
		while ($row = $result->fetch_assoc()) {
			$this->_board[] = array('name' => substr($row['CustName'], 0, 12)."..", 'values' => array($row['Account'], $row['CLI'], $row['CLD'], 
									$row['Hits'], $row['Cause']."-".$row['Reason'], $row['Vendor']));
		}
		
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>