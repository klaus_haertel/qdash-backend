<?php

class TopCustomers extends Widget
{
	
	public function __construct($nbWidget, $connection)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$now_date = $connection->makeTime();
		$last_30days_date = $connection->makeTime(0,0,-30,0,0,0);

		$this->_board = array();
								
		if ($config['testmode']) {
			// more data
			$sql = "
			Select C.name, CDR.i_customer, sum(charged_quantity)/60 as minutes 
				from CDR_Accounts as CDR,
				Customers as C
				where 
					CDR.i_env = ". $config['I_ENV'] ." and
					CDR.i_customer = C.i_customer and
					C.bill_status = 'O'
				group by C.i_customer
				order by minutes DESC Limit 10";
		}
		else {
			$sql = "
			Select C.name, CDR.i_customer, sum(charged_quantity)/60 as minutes 
				from CDR_Accounts as CDR,
				Customers as C
				where 
					CDR.i_env = ". $config['I_ENV'] ." and
					CDR.i_customer = C.i_customer and
					C.bill_status = 'O' and
					CDR.bill_time between '".$last_30days_date."' and '".$now_date."'
				group by C.i_customer
				order by minutes DESC Limit 10";
		}
		
		$result = $connection->query($sql);
		
		while ($row = $result->fetch_row()) {
			$this->_board[] = array('name' => $row['0'], 'values' => array($row['2']));
		}
		
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>