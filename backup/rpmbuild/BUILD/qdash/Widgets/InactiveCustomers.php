<?php

class InactiveCustomers extends Widget
{
	public function __construct($nbWidget, $connection)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$now_date = $connection->makeTime();
		$past_date = $connection->makeTime(0,0,'-'.$config['IC_TIME_FRAME']);
		
		$sql = "SELECT count(*)
				FROM Customers c
				WHERE NOT EXISTS (
					SELECT 1
					FROM CDR_Accounts cdr USE INDEX (env_cust_srv_time)
					WHERE cdr.i_env = c.i_env
					AND cdr.i_customer = c.i_customer
					AND cdr.i_service = 3
					AND cdr.bill_time BETWEEN '".$past_date."' AND '".$now_date."'
				)
				AND c.bill_status = 'O'
				AND c.i_env = '".$config['I_ENV']."'";
		
		$result = $connection->query($sql);
		
		while ($row = $result->fetch_row()) {
			$gaugeValue = $row['0'];
		}
		
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}

?>