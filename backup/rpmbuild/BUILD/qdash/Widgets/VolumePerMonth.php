<?php

class VolumePerMonth extends Widget
{

	private $_connection = null;
	
	public function __construct($nbWidget, $mode, $connection)
	{
		parent::__construct($nbWidget);
		/**
		mode = {ONNET, IN, OUT}
		*/
				
		$this->_data = array();
		$this->_connection = $connection;

		$timestamp = mktime(0,0,0, date("m") , date("d"), date("Y"));

		if ($mode == 'ONNET') {
			$volume = $this->performQuery('ONNETSIP') + $this->performQuery('ONNETUM'); 
		}
		else {
			$volume = $this->performQuery($mode);
		}
		
		$this->_data[] = array('timestamp' => $timestamp, 'value' => intval($volume));
		
		$this->setData($this->_data);

	}
	
	private function performQuery($mode) {
		
		/**
		mode = {ONNETSIP, ONNETUM, IN, OUT}
		*/
		
		global $config;
		
		if ($config['testmode']) {
			$thisyear = '2013';
			$thismonth = '4';
			$thisday = '19';
		}
		else {
			$thisyear = date("Y");
			$thismonth = date("m");
			$thisday = date("d") - 1;
		}
		
		switch ($mode) {
			case 'ONNETSIP':
				$sql = 
			"select sum(charged_quantity)/60 from CDR_Vendors where 
			`i_env` = '". $config['I_ENV'] ."' and 
			`bill_time` between '".$thisyear."-".$thismonth."-".$thisday." 00:00:00' and '".$thisyear."-".$thismonth."-".$thisday." 23:59:59' and 
			`i_service` = '3' and 
			`i_connection` = ". $config['INTERNAL_CNX_SIP'] ."";				
				
				break;

			case 'ONNETUM':
				$sql = 
			"select sum(charged_quantity)/60 from CDR_Vendors where 
			`i_env` = '". $config['I_ENV'] ."' and 
			`bill_time` between '".$thisyear."-".$thismonth."-".$thisday." 00:00:00' and '".$thisyear."-".$thismonth."-".$thisday." 23:59:59' and 
			`i_service` = '3' and 
			`i_connection` = ". $config['INTERNAL_CNX_UM'] ."";

				break;

			case 'IN':
				$sql = 
				"SELECT sum(V.charged_quantity)/60 FROM CDR_Vendors V, Connections C where 
				C.i_connection = V.i_connection and 	
				V.`i_env` = '". $config['I_ENV'] ."' and 
				`bill_time` between '".$thisyear."-".$thismonth."-".$thisday." 00:00:00' and '".$thisyear."-".$thismonth."-".$thisday." 23:59:59' and 
				C.`call_origin` = 'answer' and				
				`i_vendor` <> ". $config['INTERNAL_VENDOR'] ."";
				
				break;

			case 'OUT':
				$sql = 
				"select sum(V.charged_quantity)/60 from CDR_Vendors V, Connections C where 
				C.i_connection = V.i_connection and 	
				V.`i_env` = '". $config['I_ENV'] ."' and 
				`bill_time` between '".$thisyear."-".$thismonth."-".$thisday." 00:00:00' and '".$thisyear."-".$thismonth."-".$thisday." 23:59:59' and 
				`i_service` = '3' and 
				C.`call_origin` = 'answer' and				
				`i_vendor` <> ". $config['INTERNAL_VENDOR'] ."";				
								
				break;
		}
		
		$result = $this->_connection->query($sql);
		
		while ($row = $result->fetch_row()) {
			$volume = $row['0'];
		}
		
		if ($mode == 'ONNETSIP') {
			$volume = intval($volume) / 2;
		}
		
		return $volume;	
		
	}
	
}
?>