<?php

class RegisteredLocations extends Widget
{
	public function __construct($nbWidget, $connection)
	{
		parent::__construct($nbWidget);
		
		$result = $connection->query("SELECT COUNT(*) FROM location");
		
		while ($row = $result->fetch_row()) {
			$gaugeValue = $row['0'];
		}
		
		if ($this->_testmode) {
			$gaugeValue = rand(37, 198);
		}
		
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}
?>