<?php

class ConnectionUtilization extends Widget
{
	
	public function __construct($nbWidget, $connection, $mode)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$this->_board = array();
								
		$sql = "SELECT
					name, 
					description,
					capacity,
				    (count(i_active_callleg) / capacity) * 100 as 'Utilization'
				FROM
				    Connections C
				        Left join
				    Active_Calls A ON A.i_connection = C.i_connection,
				    Vendors V
				WHERE
				    C.i_env = ".$config['I_ENV']."
				    and C.call_origin = '".$mode."'
				    and V.i_vendor = C.i_vendor
				GROUP BY C.i_connection
				ORDER BY Utilization DESC";
		
		$result = $connection->query($sql);
			
		while ($row = $result->fetch_assoc()) {
			if ($row['Utilization'] > 90) $status = "red";
			elseif ($row['Utilization'] > 75) $status = "yellow";
			elseif ($row['Utilization'] > 0) $status = "green";
			else $status = "gray";	
			$this->_board[] = array('name' => $row['name']." ".$row['description'], 
									'values' => array($row['capacity'], intval($row['Utilization'])), 
									'status' => $status);
		}
		
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>