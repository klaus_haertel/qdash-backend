<?php
require_once('classes/Mandrill.php');

abstract class Widget 
{	
	/**
	 * @var resource The cURL handle that will be used
	 */
	private $_curlHandle = null;
	private $_curlPull = null;

	/**
	 * @var array The data that should be sent
	 */
	private $_data = array();
	/**
	 * @var array The alert threshold to use
	 */
	private $_thresholds = array();

	private $_nbWidget = null;
	public $_testmode = false;

	public function __construct($nbWidget)
	{	
		$this->_nbWidget = $nbWidget;
		
		global $config;

		$this->_curlHandle = curl_init(
			'https://push.ducksboard.com/values/' . $nbWidget . '/'
		);

			
		curl_setopt(
			$this->_curlHandle,
			CURLOPT_USERPWD,
			$config['API_KEY'] . ':ignored'			
		);
		
		$this->_testmode = $config['testmode'];
	}

	public function setThreshold(array $thresholds) {
		//metric direction $value
		//Ex: ASR   52 ($metric) < ($thresholds[0]->direction)  65 ($thresholds[0]->value)
		$this->_thresholds = $thresholds;
	}
	
	public function sendAlert() {

		global $config;
		
		$to_arr = array();
		array_push($to_arr, array('email' => $config['TO_EMAIL'], 'name' => $config['TO_NAME']));
		
		//class is \\Dashboard\\Widget\\NameOfWidget
		$class = explode("\\", get_class($this));
				
		$subj = $config['ALERT_SUBJ'].$class[2];
		
		$text = 'Current value: '. $this->_data['value'] ."\n" . 'Threshold value: ' . $this->_thresholds['value'];
		
		$email_arr = array('type' => 'messages', 
						'call' => 'send', 
						'message' => array('text' => $text,
											'subject' => $subj,
											'from_email' => $config['FROM_EMAIL'],
											'from_name' => $config['FROM_NAME'],
											'to' => $to_arr
							)
				);
		$json = json_encode($email_arr);	
		$ret = Mandrill::call((array) json_decode($json));
	}


	public function hasCrossedThreshold() {
		
		//get the operator
		$op = $this->_thresholds['direction'];
		
		if ($op == '<') {
			$crossed = ($this->_data['value'] < $this->_thresholds['value']);
		}
		else {
			$crossed = ($this->_data['value'] > $this->_thresholds['value']);
		}
		return ($crossed==1);
	}

	public function setData(array $data)
	{
		$this->_data = $data;
	}

	public function push()
	{
		curl_setopt(
			$this->_curlHandle,
			CURLOPT_POSTFIELDS,
			json_encode($this->_data)
		);
		print(json_encode($this->_data));
		curl_exec($this->_curlHandle);
	}

	public function curl_get($url, array $get = NULL, array $options = array()) 
	{    
	    $defaults = array( 
	        CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get), 
	        CURLOPT_HEADER => 0, 
	        CURLOPT_RETURNTRANSFER => TRUE, 
	        CURLOPT_TIMEOUT => 4 
	    ); 
	    
	    $ch = curl_init(); 
	    curl_setopt_array($ch, ($options + $defaults)); 
	    if( ! $result = curl_exec($ch)) 
	    { 
	        trigger_error(curl_error($ch)); 
	    } 
	    curl_close($ch); 
	    return $result; 
	} 

	public function pull($method, $parameter) 
	{
		global $config;
		
		$url = 'https://pull.ducksboard.com/values/' . $this->_nbWidget . '/' . $method;
		
		//echo $url;
		
		if ($method == 'since') {
			$gets = array('seconds' => $parameter);
		}
		else {
			$gets = array('count' => $parameter);
		}
		
		$options = array(CURLOPT_USERPWD => $config['API_KEY'] . ':ignored');
		$result = $this->curl_get($url, $gets, $options);
		 
		return json_decode($result,true);		
	}


	public function __destruct()
	{
		curl_close($this->_curlHandle);
	}
}
?>