<?php

class PortaSwitchDB 
{	
	/**
	 * @var resource The mysqli handle that will be used
	 */
	private $_mysqli = null;

	public function __construct($db = 'billing')
	{			
		global $config;
		
		if ($db == 'billing') {
			$this->_mysqli = new mysqli(
				$config['DB_HOST'], $config['DB_USER'], $config['DB_PASS'], 
				$config['DB_NAME'], $config['DB_PORT']
			);
		}
		else {
			$this->_mysqli = new mysqli(
				$config['DB_HOST_MS'], $config['DB_USER_MS'], $config['DB_PASS_MS'], 
				$config['DB_NAME_MS'], $config['DB_PORT_MS']
			);
		}
		/* check connection */
		if ($this->_mysqli->connect_errno) {
			printf("Connect failed: %s\n", $this->_mysqli->connect_error);
			exit();
		}
	}

	
	public function query($query)
	{
		return $this->_mysqli->query($query);
	}

	public function makeTime($year_mod = 0,$month_mod = 0,$day_mod = 0,$hr_mod = 0,$min_mod = 0,$sec_mod = 0) {
		$thisyear = date("Y");
		$thismonth = date("m");
		$thisday = date("d");
		
		$thishour = date("H");
		$thismin = date("i");
		$thissec = date("s");

		return date("Y-m-d H:i:s", mktime($thishour + $hr_mod, $thismin + $min_mod, $thissec + $sec_mod, 
										$thismonth + $month_mod, $thisday + $day_mod, $thisyear + $year_mod));

	}

	public function __destruct()
	{
		$this->_mysqli->close();
	}
}	
?>