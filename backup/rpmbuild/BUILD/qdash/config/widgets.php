<?php
//Widget specific

//NewActiveCustomers
$config['NAC_TIME_FRAME'] = 1; 

//InactiveCustomers
$config['IC_TIME_FRAME'] = 7; //no calls in 7 days

//RevenueCurrentMonth
$config['RCM_REVENUEGOAL'] = 1000; 

//FailedCalls
$config['FAILEDCALLS_TIME_FRAME'] = 600; //600 seconds, last 10 minutes

//UnusualCalls
$config['UC_TOO_SHORT'] = 10; //calls that are shorter than 10s
$config['UC_TOO_LONG'] = 7200; //calls that are longer than 2hrs

?>