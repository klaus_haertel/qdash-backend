Name:           qdash-probe
Version:        1.0
Release:        1
Summary:        The probes for the qDash dashboard application

Group:          Applications/Internet
License:        Proprietary
URL:            http://www.signalq.ca/qdash
Source0:        qdash-probe-%{version}.tar.gz

BuildArch:      noarch
Requires:       shadow-utils

%define SIGNALQ       /home/signalq
%description

%pre
getent group staff >/dev/null || groupadd -K GID_MIN=25 -K GID_MAX=30 staff
getent passwd signalq >/dev/null || \
    useradd -g staff -s /sbin/nologin \
    -c "User account, and home directory for signalQ solution software" signalq

%prep
%setup -n qdash

%build


%install
%{__rm} -rf ${RPM_BUILD_ROOT}
%{__install} -d $RPM_BUILD_ROOT%{SIGNALQ}
%{__install} -d $RPM_BUILD_ROOT%{SIGNALQ}/qdash
%{__tar} -zxvf %{SOURCE0} -C $RPM_BUILD_ROOT%{SIGNALQ}
echo "
*/10 * * * * /home/signalq/scripts/sqdash_10min.sh  #every 10min
0 * * * * /home/signalq/scripts/sqdash_1hr.sh  #every hour
0 0 * * * /home/signalq/scripts/sqdash_24hr.sh #every day
"    > ${RPM_BUILD_ROOT}%{_sysconfdir}/cron.d/qdash-cron

%post
mysql -u root mysql < $RPM_BUILD_ROOT%{SIGNALQ}/qdash/sql/create-pbslave.sql

%clean
%{__rm} -rf ${RPM_BUILD_ROOT}


%files
%defattr(-,signalq,staff,-)
%doc
%{SIGNALQ}/*
%config(noreplace) %{_sysconfdir}/cron.d/qdash-cron
%changelog


