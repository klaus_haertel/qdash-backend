#!/usr/bin/php
<?php

include_once('classes/Startup.php');

$slots = Utilities::getSlots(substr(basename(__FILE__, '.php'), strpos(basename(__FILE__, '.php'), '_')+1), $config['customer_id']);

$widgets = array();

foreach($slots as $widget) {
	$class = $widget->widget_class;	
	$widgets[] = new $class($widget->slot_id, $widget->parameter);
}

foreach ($widgets as $widget) {
	//registering thresholds
	//$widget->setThreshold($thresholds[$name]);
	
	//push data
	echo 'Pushing Widget Data for ' . get_class($widget) . '... ';
	echo $widget->push();
	echo "\n";

	//send alerts
	if ($config['send_alerts']) {
		if ($widget->hasCrossedThreshold()) {
			$widget->sendAlert();
		}
	}
}

?>