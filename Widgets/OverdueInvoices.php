<?php

class OverdueInvoices extends Widget
{
	public function __construct($nbWidget, $mode)
	{
		parent::__construct($nbWidget);
		
		global $config;
			
		if ($mode == 'overdue') {
			//invoice status = 4 (overdue)
			$status = 4;			
		}		
		else {
			//bill status = 2 (unpaid)
			$status = 2;
		}
		
		$sql = "SELECT COUNT(DISTINCT C.name) 
				FROM Customers C, Invoices I 
				WHERE C.i_customer = I.i_customer 
				AND C.i_env = :i_env 
				AND C.bill_status = 'O' 
				AND I.i_invoice_status = ".$status;
		
		$params = array('i_env' => $config['ps_env']);
		
		$result = parent::doQuery($sql, $params);
		
		$gaugeValue = $result[0];
		
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}

?>