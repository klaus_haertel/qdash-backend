<?php

class CostRevenuePerDay extends Widget
{
	private $_past_date = null;
	private $_now_date = null;
	
	public function __construct($nbWidget, $mode = 'Revenue')
	{
		//$mode is either 'revenue' or 'cost'
		
		parent::__construct($nbWidget);
		
		global $config;
		
		if ($config['testmode']) {
			$this->_past_date = '2013-08-19 00:00:00';
			$this->_now_date = '2013-08-20 00:00:00';			
		}
		else {
			$this->_past_date = Utilities::makeTime(0,0,-1, -intval(date("H")), -intval(date("i")), -intval(date("s")) );
			$this->_now_date = Utilities::makeTime(0,0,0, -intval(date("H")), -intval(date("i")), -intval(date("s")));
		}

		if ($mode == 'Revenue') {
			
			//we set csutomer rev to zero because we want to exclude subscriptions in this widget here
			$customer_revenue = 0;
			
			//get account revenue
			$account_revenue = $this->getCostRevenue('Accounts');
			
			$total_revene = (float)$customer_revenue + (float)$account_revenue;
			
			//$total_revene = bcadd($customer_revenue, $account_revenue, 4);			

			$this->setData(
				array(
					'value' => $total_revene, 'timestamp' => mktime($this->_now_date)
				)
			);
		}
		else {
			//mode = cost
			$cost = $this->getCostRevenue('Vendors');

			$this->setData(
				array(
					'value' => $cost, 'timestamp' => mktime($this->_now_date)
				)
			);
		}

	}
	
	private function getCostRevenue($table) {

		//get currencies and exchange rates for yesterday
		return $this->performQuery($table, $this->getCurrencies());
	}

	private function getCurrencies() {
		
		global $config;
		//get currencies and exchange rates for yesterday
		$currencies = array();
		
		$sql = "SELECT timestamp, iso_4217, base_iso_4217, base_units 
				FROM `porta-billing`.X_Rates 
				where i_env= :i_env 
				AND timestamp between :past_date and :now_date";
		
		$params = array('i_env' => $config['ps_env'],
						'past_date' => $this->_past_date,
						'now_date' => $this->_now_date,
		);
		
		$result = parent::doQueryAll($sql, $params);
		
		$currencies = Utilities::array_column($result, 'base_units', 'iso_4217');
		
		return $currencies;
	}
		
	private function performQuery($table, $currencies) {
		/**
		i_service = 1 - Credits/Refunds
		i_service = 2 - Payments
		i_service = 11 - Cleanup
		i_service = 1 - Taxes
		i_service = 4 - Subscriptions
		*/
		global $config;
		
		switch ($table) {
			case 'Accounts':
			case 'Customers':
				$sql = "SELECT 
							iso_4217, sum(charged_amount) as total 
						FROM 
							CDR_".$table." cdr, 
							Customers C 
					WHERE 
						cdr.i_env = :i_env
						AND cdr.i_service NOT IN (1, 11, 2, 12, 4)
						AND cdr.i_customer = C.i_customer
						AND C.bill_status = 'O'
						AND cdr.bill_time between :past_date and :now_date
					GROUP BY iso_4217";
				break;
			case 'Vendors':
				$sql = "
						SELECT 
						    iso_4217, sum(charged_amount) as total
						FROM
						    CDR_Vendors cdr,
						    Vendors V
						where
						    cdr.i_env = :i_env
						    AND cdr.i_service NOT IN (1 , 11, 2, 12)
						    AND cdr.i_vendor = V.i_vendor
						    AND cdr.bill_time between :past_date and :now_date
						GROUP BY iso_4217";			
		}
		
		$params = array('i_env' => $config['ps_env'],
						'past_date' => $this->_past_date,
						'now_date' => $this->_now_date,
		);

		//get all revenue/cost items grouped by currency
		$result = parent::doQueryAll($sql, $params);
		
		$revenue = 0;
		
		if(!is_null($result)) {
			while (list($key, $revenue_item) = each($result)) {
				
				//get exchange rate from earlier
				if (!array_key_exists($revenue_item['iso_4217'], $currencies )) {
					//if there is no entry in currencies array, this means we're dealing with base currency
					$x_rate = 1;				
				}
				else {
					$x_rate = $currencies[$revenue_item['iso_4217']]['base_units'];
				}
				
				//get total by country
				$total = $revenue_item['total'];
					
				if (settype($x_rate, "float") && settype($total, "float")) {
					$revenue_per_currency =  $x_rate * $total;	
				}
				else $revenue_per_currency = 0;
				
				$revenue = $revenue + round($revenue_per_currency, 4);		
			}			
		}
	
		return $revenue;		
	}
	
}
?>