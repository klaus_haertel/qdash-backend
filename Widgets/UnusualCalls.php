<?php

class UnusualCalls extends Widget
{
	
	public function __construct($nbWidget, $mode)
	{
		//$mode can be "long" or "short" calls
		parent::__construct($nbWidget);
		
		global $config;
		
		$this->_board = array();
		
		switch ($mode) {
			case 'long':
				//build the SQL query part
				$duration_operator = "> '".$config['UC_TOO_LONG']."'";
				break;
			case 'short':
				//build the SQL query part
				$duration_operator = "between '0.1' and '".$config['UC_TOO_SHORT']."'";
				break;
		}
				
		$sql = "
				SELECT 
				    D.destination as Destination,
				    count(*) as NrOfCalls,
				    avg(charged_quantity) as ACD,
				    V.name as Vendor
				FROM
				    CDR_Vendors CDRV
				        JOIN
				    Connections C ON CDRV.i_connection = C.i_connection
				        JOIN
				    Vendors V ON V.i_vendor = CDRV.i_vendor
				        JOIN
				    Destinations D ON D.i_dest = CDRV.i_dest
				WHERE
				        charged_quantity ".$duration_operator."
				        and C.call_origin = 'originate'
				        and CDRV.i_env = :i_env
				group by CDRV.i_vendor , CDRV.i_dest
				Order by NrOfCalls DESC
		";
						
		$params = array('i_env' => $config['ps_env']);
						
		$result = parent::doQuery($sql, $params);
				
		if(!is_null($result)) {
			$this->_board[] = array('name' => $result['NrOfCalls'], 
									'values' => array($result['Destination'], gmdate("H:i:s",intval($result['ACD'])), $result['Vendor']) );
		}
		
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>