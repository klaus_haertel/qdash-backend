<?php

class TopCustomers extends Widget
{
	
	public function __construct($nbWidget)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$now_date = Utilities::makeTime();
		$last_30days_date = Utilities::makeTime(0,0,-30,0,0,0);

		$this->_board = array();
								
		if ($config['testmode']) {
			// more data
			$timeframe = "";
		}
		else {
			$timeframe = "and CDR.bill_time between " . $last_30days_date." and ".$now_date;			
		}
		
		$sql = "
			Select C.name, CDR.i_customer, sum(charged_quantity)/60 as minutes 
				from CDR_Accounts as CDR,
				Customers as C
				where 
					CDR.i_env = :i_env and
					CDR.i_customer = C.i_customer and
					C.bill_status = 'O'"
				. $timeframe .	
				"group by C.i_customer
				order by minutes DESC Limit 10";
		
		$params = array('i_env' => $config['ps_env']);
		
		$result = parent::doQuery($sql, $params);
		
		if(!is_null($result)) {
			$this->_board[] = array('name' => $result[0], 'values' => array($result[2]));
		}
		
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>