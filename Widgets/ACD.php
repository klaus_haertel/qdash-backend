<?php

class ACD extends Widget
{
		public function __construct($nbWidget, $mode) {
		
		parent::__construct($nbWidget);

		global $config;
		
		$thisyear = date("Y");
		$thismonth = date("m");
		$thisday = date("d");

		$now_date = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), $thismonth,$thisday,$thisyear));
		$last_hour_date = date("Y-m-d H:i:s", mktime(date("H")-1, date("i"), date("s"), $thismonth,$thisday,$thisyear));

		// in testmode we need more data
		
		if ($config['testmode']) {
			$now_date = "2013-04-30 00:00:00";
			$last_hour_date = "2013-04-01 00:00:00";
		}

		$sql = "
				SELECT 
				    AVG(charged_quantity) as ACD
				FROM
				    CDR_Vendors CDR
				        LEFT JOIN
				    Connections C ON C.i_connection = CDR.i_connection
				WHERE
				    CDR.i_env = :i_env 
				    	and charged_quantity > 0
				        and i_service = 3
				        and bill_time between :last_hour_date and :now_date
				        and call_origin = :mode
				        and C.i_connection <> :cnx_sip 
						and C.i_connection <> :cnx_um";

		$params = array('i_env' => $config['ps_env'], 
						'last_hour_date' => $last_hour_date, 
						'now_date' => $now_date,
						'mode' => $mode,
						'cnx_um' => $config['ps_internal_um'],
						'cnx_sip' => $config['ps_internal_sip'],
						);

		$result = parent::doQuery($sql, $params);
		
		if ($result['ACD'] <> '') $acd = intval($result['ACD']);
				else $acd = 0;

		$this->setData(array('value' => $acd));
	}
}

?>
