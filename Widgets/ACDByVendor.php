<?php

class ACDByVendor extends Widget
{
	
	public function __construct($nbWidget)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		if ($this->_testmode) {
			//more data in testmode
			$now_date = Utilities::makeTime();
			$past_date = "2000-01-01";	
		}
		else {
			$now_date = Utilities::makeTime();
			$past_date = Utilities::makeTime(0,0,0,0,0,'-'.$config['ACD_BY_VENDOR_TIME_FRAME']);
		}
		
		$this->_board = array();
		
		if($config['SHOW_INTERNAL_CNX']) {
			$exclude_internal_cnx = ''; 
		}
		else {
			$exclude_internal_cnx = 'and V.i_vendor <> '.$config['ps_internal_vendor'];
		}
								
		$sql = "
				SELECT 
				    V.name as VName,
				    C.description as CName,
				    avg(charged_quantity) as ACD,
				    avg(setup_time) as PDD,
				    count(CDR.id) as Calls,
				    sum(charged_quantity) as Total
				FROM
				    CDR_Vendors CDR
				        LEFT JOIN
				    Connections C ON C.i_connection = CDR.i_connection
				        LEFT JOIN
				    Vendors V ON V.i_vendor = C.i_vendor
				WHERE
				    CDR.i_env = :i_env
				    and charged_quantity > 0
				    and i_service = 3
				    and call_origin = 'originate'
				    and bill_time between :past_date and :now_date
				    :exclude_internal_sip
				GROUP BY C.i_connection, C.i_vendor";
		
		$params = array('i_env' => $config['ps_env'],
						'past_date' => $past_date,
						'now_date' => $now_date,
						'exclude_internal_sip' => $exclude_internal_cnx,
		);
		
		$result = parent::doQuery($sql, $params);
		
		$pdd = $result['PDD'];
		if ($pdd > $config['PDD_RED_THESHOLD']) $status = "red";
		elseif ($pdd > $config['PDD_YELLOW_THESHOLD']) $status = "yellow";
		elseif ($pdd > 0) $status = "green";
			else $status = "gray";	

		$this->_board[] = array('name' => $result['VName'].'-'.$result['CName'], 
							'values' => array($result['Calls'], 
											gmdate("H:i:s",intval($result['Total'])), 
											gmdate("H:i:s", intval($result['ACD'])), 
											intval($result['PDD'])), 
							'status' => $status);
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>