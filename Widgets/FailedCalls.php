<?php

class FailedCalls extends Widget
{
	
	public function __construct($nbWidget)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		if ($this->_testmode) {
			//more data in testmode
			$now_date = Utilities::makeTime();
			$past_date = "2000-01-01";	
		}
		else {
			$now_date = Utilities::makeTime();
			$past_date = Utilities::makeTime(0,0,0,0,0,'-'.$config['FAILEDCALLS_TIME_FRAME']);
		}
		
		$this->_board = array();
								
		$sql = "
				SELECT count(VF.disconnect_cause) as Hits, C.name as CustName, VF.account_id as Account, VF.CLI as CLI, 
				VF.CLD as CLD, VF.disconnect_cause as Cause, disconnect_text as Reason, V.name as Vendor, Countries.name as Country
				FROM  CDR_Vendors_Failed VF, Customers C, Disconnect_Reasons DR, Vendors V, Destinations D, Countries
					WHERE VF.i_env= :i_env
					AND VF.i_customer = C.i_customer
					AND VF.disconnect_cause = DR.disconnect_cause
					AND VF.i_vendor = V.i_vendor
					AND VF.i_dest = D.i_dest
					AND D.iso_3166_1_a2 = Countries.iso_3166_1_a2
					AND bill_time between :past_date AND :now_date
					GROUP BY C.name, VF.account_id, VF.disconnect_cause
					Order by Hits DESC		
		";
		
		$params = array('i_env' => $config['ps_env'],
						'past_date' => $past_date,
						'now_date' => $now_date,
		);
		
		$result = parent::doQuery($sql, $params);
		
		if(!is_null($result)) {
			$this->_board[] = array('name' => substr($result['CustName'], 0, 12)."..", 
									'values' => array($result['Account'], 
														$result['CLI'], $result['CLD'], 
														$result['Hits'], $result['Cause']."-".$result['Reason'], 
														$result['Vendor']));
		}
		
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>