<?php

class ActiveCallsByCustomer extends Widget
{
	public function __construct($nbWidget, $i_customer)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$sql = "SELECT COUNT(*) FROM Active_Calls WHERE i_env = :i_env and i_customer = :i_customer";
		
		echo 'env'.$config['ps_env'].' '.$i_customer;
		
		$result = parent::doQuery($sql, array('i_env' => $config['ps_env'], 'i_customer' => $i_customer));
		$gaugeValue = $result[0];
		
		if ($this->_testmode) {
			$gaugeValue = rand(37, 198);
		}
		
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}
?>