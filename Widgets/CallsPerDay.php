<?php

class CallsPerDay extends Widget
{
	public function __construct($nbWidget)
	{
		parent::__construct($nbWidget);
				
		global $config;
				
		$thisyear = date("Y");
		$thismonth = date("m");
		$thisday = date("d");
		
		$today = date("Y-m-d", mktime(0,0,0,$thismonth,$thisday,$thisyear));
		
		$this->_data = array();
		
		$today_start = $today. '00:00:00';
		$today_end = $today. '23:59:59';
						
		$sql = "SELECT Count(*) FROM CDR_Vendors 
		WHERE i_env = :i_env 
		AND bill_time between :today_start and :today_end  
		AND i_service = '3'";
		
		$params = array('i_env' => $config['ps_env'],
						'today_start' => $today_start,
						'today_end' => $today_end,
		);
							
		$result = parent::doQuery($sql, $params);
				
		$gaugeValue = $result[0];
		
		if ($config['testmode']) {
			$gaugeValue = rand(50000,95000);
		}

		$timestamp = mktime(0,0,0, $thismonth, $thisday, $thisyear);		
		$this->_data[] = array('timestamp' => $timestamp, 'value' => $gaugeValue);
		
		$this->setData($this->_data);

	}
}


?>