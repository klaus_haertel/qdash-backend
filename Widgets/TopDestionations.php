<?php

class TopDestinations extends Widget
{
	
	public function __construct($nbWidget)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$this->_board = array();
		
		if ($this->_testmode) {
			//more data in testmode
			$now_date = Utilities::makeTime();
			$past_date = "2000-01-01";	
		}
		else {
			$now_date = Utilities::makeTime();
			$past_date = Utilities::makeTime(0,0,0,0,0,'-'.$config['TOP_DESTINATIONS_TIME_FRAME']);
		}

		$sql = "
				SELECT 
				    D.destination Destination, count(*) as NrOfCalls
				FROM
				    CDR_Vendors CDR
				        JOIN
				    Destinations D ON CDR.i_dest = D.i_dest
				WHERE
				    CDR.i_env = :i_env
					and i_service = 3
					and bill_time between :past_date and :now_date
				GROUP BY CDR.i_dest
				ORDER BY NrOfCalls DESC
		";
		
		$params = array('i_env' => $config['ps_env'],
						'past_date' => $past_date,
						'now_date' => $now_date,
		);
						
		$result = parent::doQuery($sql, $params);
				
		if(!is_null($result)) {
			$this->_board[] = array('name' => $result['Destination'], 
									'values' => array($result['NrOfCalls']));
		}
		
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
	}
}
?>