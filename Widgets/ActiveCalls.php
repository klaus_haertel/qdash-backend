<?php

class ActiveCalls extends Widget
{
	public function __construct($nbWidget)
	{
		parent::__construct($nbWidget);
		
		global $config;
		
		$sql = "SELECT COUNT(*) FROM Active_Calls WHERE i_env = :i_env";
		
		$result = parent::doQuery($sql, array('i_env' => $config['ps_env']));
		$gaugeValue = $result[0];
		
		if ($this->_testmode) {
			$gaugeValue = rand(37, 198);
		}
		
		$this->setData(
			array(
				'value' => $gaugeValue
			)
		);
	}
}
?>