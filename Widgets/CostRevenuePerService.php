<?php

class CostRevenuePerService extends Widget
{
	private $_connection = null;
	
	public function __construct($nbWidget)
	{		
		parent::__construct($nbWidget);
		
		global $config;
		$this->_board = array();
		
		if ($config['testmode']) {
			$yesterday = '2013-04-19 00:00:00';
			$today = '2013-04-20 00:00:00';			
		}
		else {
			$first = date("Y-m-d", mktime(0,0,0,date("m"), 1, date("Y")));
			$today = $connection->makeTime();
		}

		//get account revenue
		$acct_revenue = $this->getCostRevenue('Accounts', $yesterday, $today);
			
		//get customer revenue
		$cust_revenue = $this->getCostRevenue('Customers', $yesterday, $today);	
		
		$total_revenue = $this->addRevenues($acct_revenue, $cust_revenue);
		
		//mode = cost
		$cost = $this->getCostRevenue('Vendors', $yesterday, $today);

		$total_revenue_cost = $this->mergeRevenueCost($total_revenue, $cost);

		print_r($total_revenue_cost);

		$_board = array();
		
		foreach($total_revenue as $reveue) {
			
			$_board = array('name' => $revenue['SName'],
							'values' => array($revenue['totalRev']),
							'status' => $status);
		}
		
		/**
		$this->setData(
			array(
				'value' => array('board' => $this->_board)
			)
		);
		*/

	}
	
	private function addRevenues($acct_revenue, $cust_revenue) {
		$total_revenue = $acct_revenue;
		$new_service = false;
		
		foreach ($cust_revenue as $revenue) {
			$new_item = true;
			
			foreach($total_revenue as $key => $total) {
				//test whether the same service exists in customer and account revenues, if yes add up the values
				if($revenue['SName'] == $total['SName']) {
					$new_service = false;
					//echo 'same service detected:' . $revenue['SName'] . "\n"; 
					$total_revenue[$key]['totalRev'] = $total_revenue[$key]['totalRev'] + $revenue['totalRev']; 
					$total_revenue[$key]['Trx'] = $total_revenue[$key]['Trx'] + $revenue['Trx'];
					break;
				}
				else {
					$new_service = true;
				}
				
			}
			if ($new_item && $new_service) {
				$total_revenue[] = $revenue;
			} 
		}
		return $total_revenue;	
	}

	private function mergeRevenueCost($total_revenue, $costs) {
		
		$total_revenue_cost = $total_revenue;
		$new_service = false;
		
		foreach ($costs as $cost) {
			$new_item = true;
			
			foreach($total_revenue_cost as $key => $total) {
				//test whether the same service exists in total revenue and cost
				if($cost['SName'] == $total['SName']) {
					$new_service = false;
					//echo 'same service detected:' . $revenue['SName'] . "\n"; 
					$total_revenue_cost[$key]['Cost'] = $cost['totalRev']; 
					break;
				}
				else {
					$new_service = true;
				}
				
			}
			if ($new_item && $new_service) {
				
				//$total_revenue[] = $revenue;
			} 
		}
		return $total_revenue_cost;	
	}

	
	private function getCostRevenue($table, $past, $today) {

		//get currencies and exchange rates for yesterday

		$currencies = $this->getCurrencies($past, $today);
		$revenue = $this->performQuery($table, $currencies, $past, $today);
		
		return $revenue;
	}

	private function getCurrencies($past, $today) {
		
		global $config;
		//get currencies and exchange rates for yesterday
		$currencies = array();
		
		$sql = "SELECT timestamp, iso_4217, base_iso_4217, base_units 
				FROM `porta-billing`.X_Rates 
				where i_env= ".$config['ps_env']." 
				AND timestamp between '".$past."' and '".$today."'";
		
		$result = $this->_connection->query($sql);

		while ($row = $result->fetch_assoc()) {
			$currencies[$row['iso_4217']] = $row;
		}
		
		return $currencies;
	}
		
	private function performQuery($table, $currencies, $past, $today) {
		global $config;
		
		switch ($table) {
			case 'Accounts':
				$sql = "
					SELECT 
						s.name as SName,
					    iso_4217,
					    sum(charged_amount) as totalRev,
					    sum(charged_quantity) as totalCharged,
						sum(used_quantity) as totalUsed,
					    count(*) as NrOfTrx
					FROM
					    CDR_Accounts cdr
					        JOIN
					    customers c ON cdr.i_customer = c.i_customer
					        JOIN
					    Services S ON cdr.i_service = S.i_service
					where
					    cdr.i_env = ".$config['ps_env']." 
					    and c.bill_status = 'O'
					    and cdr.bill_time between '".$past."' and '".$today."'
					group by S.name , iso_4217
				";
				break;
			case 'Customers':
				$sql = "
					SELECT 
						s.name as SName,
					    iso_4217,
					    sum(charged_amount) as totalRev,
					    sum(charged_quantity) as totalCharged,
						sum(used_quantity) as totalUsed,
					    count(*) as NrOfTrx
					FROM
					    CDR_Customers cdr
					        JOIN
					    customers c ON cdr.i_customer = c.i_customer
					        JOIN
					    Services S ON cdr.i_service = S.i_service
					where
					    cdr.i_env = ".$config['ps_env']." 
					    and c.bill_status = 'O'
					    and cdr.bill_time between '".$past."' and '".$today."'
					group by S.name , iso_4217
				";
				break;
			case 'Vendors':
				$sql = "
						SELECT 
						    s.name as SName,
						    iso_4217, 
						    sum(charged_amount) as totalRev,
						    sum(charged_quantity) as totalCharged,
						    sum(used_quantity) as totalUsed,
						    count(*) as NrOfTrx
						FROM
						    CDR_Vendors cdr
						        JOIN
						    Vendors V ON cdr.i_vendor = V.i_vendor
						        JOIN
						    Services S ON cdr.i_service = S.i_service
						where
						    cdr.i_env = ".$config['ps_env']."
						        and cdr.bill_time between '".$past."' and '".$today."'
						group by S.name, iso_4217";
				break;			
		}

		$result = $this->_connection->query($sql);
		
		$board = array();
		$index = 0;
		$revenue = 0;
		
		while ($row = $result->fetch_assoc()) {
			
			//get exchange rate from earlier
			if ($currencies[$row['iso_4217']] == '') {
				//if there is no entry in currencies array, this means we're dealing with base currency
				$x_rate = 1;
			}
			else $x_rate = $currencies[$row['iso_4217']]['base_units'];

			//get total by country
			$total = $row['totalRev'];
			
			//calculate revenue in base currency	
			if (settype($x_rate, "float") && settype($total, "float")) {
				$rev_in_base_currency =  $x_rate * $total;	
			}
			else $rev_in_base_currency = 0;	
			
			$revenue = $revenue + round($rev_in_base_currency, 4);	
			
			
			//check if we already have a service item, if not add it to our result array
			if($index == 0) {
				$board[$index] = array('SName' => "Total", 'totalRev' => $revenue);
				$index++;
				$board[$index] = array('SName' => $row['SName'], 'totalRev' => $rev_in_base_currency, 'Trx' => $row['NrOfTrx']);
			}
			else {
				//check if we have the same service (maybe in a different currency
				if($board[$index-1]['SName'] == $row['SName']) {
					//update the total
					$board[0]['totalRev'] = $board[0]['totalRev'] + $rev_in_base_currency; 
					//update the service revenue
					$board[$index-1]['totalRev'] = $board[$index-1]['totalRev'] + $rev_in_base_currency;
					$board[$index-1]['Trx'] = $board[$index-1]['Trx'] + $row['NrOfTrx'];
				}
				else {
					//update the total
					$board[0]['totalRev'] = $board[0]['totalRev'] + $rev_in_base_currency; 
					//add new service
					$board[$index] = array('SName' => $row['SName'], 'totalRev' => $rev_in_base_currency, 'Trx' => $row['NrOfTrx']);	
				}
			}	
			$index++;
		}
				
		return $board;		
	}
	
}
?>